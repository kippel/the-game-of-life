extends Camera2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var speed : int = 200
## https://www.youtube.com/watch?v=FSoFJAmh96g
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var x = int(Input.is_action_pressed("ui_d")) - int(Input.is_action_pressed("ui_a"))
		
	var y = int(Input.is_action_pressed("ui_s")) - int(Input.is_action_pressed("ui_w"))
	
	position.x += x * speed * delta
	position.y += y * speed * delta	
		
	
