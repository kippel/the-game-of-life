extends Control


var pauseLife : bool = false

onready var slider := $ColorRect/VBoxContainer/HBoxContainer2/HSlider
onready var w := $ColorRect/VBoxContainer/HBoxContainer3/LineEdit
onready var h := $ColorRect/VBoxContainer/HBoxContainer3/LineEdit2

signal pauseLifes(index)
signal randomLife()
signal okLife()
signal okSlider(index)
signal whLife()

func _ready():
	
	slider.value = 1


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

## Slider

func _on_pauseLife_pressed():
	pauseLife = !pauseLife
	
	emit_signal("pauseLifes", pauseLife)
	


func _on_random_pressed():
	emit_signal("randomLife")


func _on_ok_pressed():
	emit_signal("okLife")
	


func _on_ok_button_pressed():
	var d = slider.value
	print(d)
	emit_signal("okSlider", d)
	


func _on_size_pressed():
	pass # Replace with function body.
	
	print(w.text)
	print(h.text)
	
	Gloca.WIDTH = w.text
	Gloca.HEIGHT = h.text
	
	emit_signal("whLife")
