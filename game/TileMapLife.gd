extends Node2D


onready var timer_life := $timer_life

#onready var lef := $left

func _ready():

	left_right()
	

func _on_LifeC_pauseLifes(index):
	$TileLife.room(index)
	
func _on_LifeC_randomLife():
	$TileLife.random_life()

func _on_LifeC_okLife():
	$TileLife.random_ok()

func _on_LifeC_okSlider(index):
	timer_life.set_wait_time(index)
	

func left_right() -> void:
	$left.clear()
	$right.clear()
	for x in range(Gloca.WIDTH):
		for y in range(Gloca.HEIGHT):
			$left.set_cell(x, y, 0)
	
			$right.set_cell(x, y, 0)


func _on_LifeC_whLife():
	$TileLife.map_o()
	
	left_right()
	
