extends TileMap

var TILE_SIZE : int = 32

var temp_field : Array = []
var pause : bool = false

onready var timer_life = get_node("../timer_life")

# Called when the node enters the scene tree for the first time.
func _ready():
	map_o()
	
	
	
	timer_life.stop()
	print("foo")
	
func room(index : bool) -> void:
	
	pause = index
	_timer_e()
	
	
func _timer_e() -> void:
	if pause:
		timer_life.start()
		_line()	
	else:
		timer_life.stop()


func random_ok() -> void:
	for x in range(Gloca.WIDTH):	
		for y in range(Gloca.HEIGHT):
			set_cell(x, y, 0)
			temp_field[x][y] = 0
	
func random_life() -> void:
	
	random_ok()
	
	randomize()
	
	var list : Array = [0, 1]
	for x in range(Gloca.WIDTH):	
		for y in range(Gloca.HEIGHT):
			var n = list[randi() % list.size()]
			set_cell(x, y, n)
			temp_field[x][y] = n

func _input(event):
	
	if event.is_action_pressed("ui_pause"):
		
		pause = !pause
		
		_timer_e()
		
			
	if event.is_action_pressed("ui_e"):
	
			
		var pos = (get_local_mouse_position()/TILE_SIZE).floor()
		
		
		if (0 <= pos[0] && Gloca.WIDTH > pos[0] && 0 <= pos[1] && Gloca.HEIGHT > pos[1]):
			#0<pos[0] or Gloca.WIDTH < pos[0] && 0 < pos[1] or Gloca.HEIGHT < pos[1])
		
		
			if temp_field[pos[0]][pos[1]] != 0:
				set_cell(pos[0], pos[1], 0)
				temp_field[pos[0]][pos[1]] = 0
			else: 
				set_cell(pos[0], pos[1], 1)		
				temp_field[pos[0]][pos[1]] = 1
			
func map_o():
	clear()
	temp_field = [] 
	for x in range(Gloca.WIDTH):
		var temp : Array = []
		for y in range(Gloca.HEIGHT):
			set_cell(x, y, 0)
			temp.append(0)
		temp_field.append(temp)


func _line() -> void:
	
	for x in range(Gloca.WIDTH):
		for y in range(Gloca.HEIGHT):
			var live = 0
			for x_off in [-1, 0, 1]:
				for y_off in [-1, 0, 1]:
					if x_off != y_off or x_off != 0:
						if get_cell(x+x_off, y+y_off) == 1:
							live += 1
							
			if get_cell(x, y) == 1:
				if live in [2, 3]:
					temp_field[x][y] = 1
				else:
					temp_field[x][y] = 0
			else:
				if live == 3:
					temp_field[x][y] = 1
				else:
					temp_field[x][y] = 0
					
	for x in range(Gloca.WIDTH):
		for y in range(Gloca.HEIGHT):
			set_cell(x, y, temp_field[x][y])



func _on_timer_life_timeout():
	_line()	
	print("foo bar")
